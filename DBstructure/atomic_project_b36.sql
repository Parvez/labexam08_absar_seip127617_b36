-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 20, 2016 at 09:14 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomic_project_b36`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE `birthday` (
  `id` int(5) NOT NULL,
  `username` varchar(100) NOT NULL,
  `birthday` varchar(20) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `username`, `birthday`, `is_delete`) VALUES
(1, 'Tushar Chy', '11/1/1111', 0),
(2, 'Tushar Chy', '11/1/1111', 0),
(3, 'Tushar Chy', '11/1/1111', 0),
(4, 'shamrat', '9-9-95', 0),
(5, 'dscns`', '234567', 0),
(6, 'sham', 'yss', 0),
(7, 'shamrat', '12-12-12', 0);

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE `book_title` (
  `id` int(5) NOT NULL,
  `book_title` varchar(100) NOT NULL,
  `author_name` varchar(100) NOT NULL,
  `is_delete` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_title`, `author_name`, `is_delete`) VALUES
(1, '', '', 1),
(2, '', '', 0),
(3, '', '', 1),
(4, 'PHP', 'Yasin', 0),
(6, 'HTML', 'Gazi', 0),
(8, 'Advance PHP', 'shamrat', 0),
(9, 'sha', 'Mha', 1),
(10, 'MC', 'MM', 1);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(5) NOT NULL,
  `username` varchar(100) NOT NULL,
  `city_name` varchar(100) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `username`, `city_name`, `is_delete`) VALUES
(1, 'shamrat1', 'chittagong', 0),
(2, 'Tishar Chy', 'Chittagong', 0),
(3, 'Maruf Hossain', 'Noakhali', 0),
(4, 'Maruf Hossain', 'Noakhali', 0),
(5, 'shamrat1', 'chittagong', 0),
(6, 'siham', 'chittagong', 0),
(7, 'ss', 'barisal', 0),
(8, 'yshamrat@gmail.com', 'chittagong', 0);

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(5) NOT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `username`, `email`, `is_delete`) VALUES
(1, 'Mahmud Sabuj', 'mahmud@gmail.com', 0),
(2, 'Tushar Chy', 'tushar.ctg@gmail.com', 1),
(3, 'shamrat', 'yshamrat@gmail.com', 0);

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` int(5) NOT NULL,
  `username` varchar(100) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `username`, `gender`, `is_delete`) VALUES
(1, 'Tushar Chy', 'male', 0),
(2, 'Rokeya Begum', 'female', 0),
(3, 'Yasin Siraj shamrat', 'male', 0);

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE `hobbies` (
  `id` int(5) NOT NULL,
  `username` varchar(100) NOT NULL,
  `hobbies` varchar(200) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `username`, `hobbies`, `is_delete`) VALUES
(1, 'Akkas Ali', 'playing, cooking, riding', 0),
(2, 'Tushar Ali', 'Reading, Teaching', 0),
(3, 'shamrat', 'programming,dancing,singing,drawing', 0),
(4, 'Yasin Siraj shamrat', 'Playing Cricket,Gaming,Watching Movies,Hanging Out', 0),
(5, 'Emon', 'Playing Cricket,Hanging Out', 0);

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE `profile_picture` (
  `id` int(5) NOT NULL,
  `username` varchar(100) NOT NULL,
  `pro_pic` varchar(200) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_picture`
--

INSERT INTO `profile_picture` (`id`, `username`, `pro_pic`, `is_delete`) VALUES
(3, 'shamrat', '1479352437wallpapers-44.jpg', 0),
(4, 'Yasin shamrat ', '1479308757wallpapers-11.jpg', 0),
(6, 'samy', '1478966778toUploadImg.jpg', 1),
(7, 'yasin', '1479007191wallpapers-05.jpg', 1),
(8, 'shamrat', '1479182772wallpapers-07.jpg', 1),
(9, 'shamrat', '1479303707wallpapers-44.jpg', 1),
(10, 'shamrat', '1479308733wallpapers-06.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `summary_of_organization`
--

CREATE TABLE `summary_of_organization` (
  `id` int(5) NOT NULL,
  `name` varchar(100) NOT NULL,
  `summery` varchar(500) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summary_of_organization`
--

INSERT INTO `summary_of_organization` (`id`, `name`, `summery`, `is_delete`) VALUES
(1, 'Bangladesh Police', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop p', 0),
(2, 'Bangladesh ideal college', 'BIC', 0),
(3, 'JBL', 'Sound system', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
