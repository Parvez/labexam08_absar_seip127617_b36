<?php

namespace App\City;

use App\Database as DB;
use PDO;
use PDOException;
use App\Message\Message;
use App\Utility\Utility;
class City extends DB
{

    public $id = "";

    public $name = "";

    public $City = "";


    public function __construct()
    {

        parent::__construct();

    }
    public function index($Mode="ASSOC"){

        $STH = $this->conn->query('SELECT * FROM `city` WHERE `is_delete`=0');


        if($Mode=="OBJ")   $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();

        return $arrAllData;


    }
    public function view(){
        $STH = $this->conn->query('SELECT * from city WHERE `id`='.$this->id);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $objAllData = $STH->fetchAll();
        return $objAllData;
    }

    public function update(){

        $data = array($this->name,$this->City,$this->id);
        $STH = $this->conn->prepare("UPDATE `city` SET `username` =? , `city_name` =?  WHERE `id` =?");
        $STH->execute($data);

        Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ City: $this->City ] <br> Data Has Been Updated Successfully!</h3></div>");
        Utility::redirect('index.php');
    }

    public function setData($data = NULL){
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        if(array_key_exists('username',$data)){
            $this->name = $data['username'];
        }
        if(array_key_exists('city',$data)){
            $this->City = $data['city'];
        }
    }
    public function trash()
    {
        try {
            $query = "UPDATE `city` SET `is_delete` = ? WHERE `city`.`id` = ?";
            $STH = $this->conn->prepare($query);
            $STH->execute(array(1, $this->id));

            if ($STH) {
                Message::message("<div id='msg'></div><h3 align='center'> Data Has Been deleted Successfully!</h3></div>");
                Utility::redirect('index.php');
            }
        } catch (PDOException $e) {
            echo 'Error:' . $e->getMessage();
        }
    }
    public function delete(){
        $DBH = $this->conn;
        $STH = $DBH->prepare('DELETE from `city` WHERE `id`='.$this->id);
        $STH->execute();

        Message::message("<div id='msg'></div><h3 align='center'> <br> Data Has Been Deleted Successfully!</h3></div>");


        Utility::redirect('index.php');
    }

    public function store(){
        $DBH = $this->conn;
        $data = array($this->name,$this->City);
        $STH = $DBH->prepare("INSERT INTO `city`(`id`, `username`, `city_name`) VALUES (NULL ,?,?)");
        $STH->execute($data);

        Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ City: $this->City ] <br> Data Has Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');


    }


}