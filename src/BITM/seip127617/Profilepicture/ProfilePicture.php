<?php

namespace App\ProfilePicture;

use App\Database as DB;

use PDO;
use App\Message\Message;
use App\Utility\Utility;
class ProfilePicture extends DB
{
    public $id;
    public $name;
    public $picture;

    public function __construct()
    {

        parent::__construct();

    }
    public function index($Mode="ASSOC"){

        $STH = $this->conn->query('SELECT * FROM `profile_picture` WHERE `is_delete`=0');


        if($Mode=="OBJ")   $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();

        return $arrAllData;


    }
    public function view(){
        $STH = $this->conn->query('SELECT * from profile_picture WHERE `id`='.$this->id);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $objAllData = $STH->fetchAll();
        return $objAllData;
    }

    public function update(){

        $data = array($this->name,$this->picture,$this->id);
        $STH = $this->conn->prepare("UPDATE `profile_picture` SET `username` = ? , `pro_pic` = ? WHERE `id` =?");
        $STH->execute($data);

        Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ Picture: $this->picture ] <br> Data Has Been Updated Successfully!</h3></div>");


        Utility::redirect('index.php');
    }
    public function trash(){
        try{
            $query = "UPDATE `profile_picture` SET `is_delete` = ? WHERE `profile_picture`.`id` = ?";
            $STH = $this->conn->prepare($query);
            $STH->execute(array(1,$this->id));

            if($STH){
                Message::message("<div id='msg'></div><h3 align='center'> Data Has Been deleted Successfully!</h3></div>");
                Utility::redirect('index.php');
            }
        }
        catch(PDOException $e){
            echo 'Error:'.$e->getMessage();
        }}

    public function setData($data = NULL){
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name = $data['name'];
        }
        if(array_key_exists('image',$data)){
            $this->picture = $data['image'];
        }
    }
    public function delete(){
        $DBH = $this->conn;
        $STH = $DBH->prepare('DELETE from `profile_picture` WHERE `id`='.$this->id);
        $STH->execute();

        Message::message("<div id='msg'></div><h3 align='center'> <br> Data Has Been Deleted Successfully!</h3></div>");


        Utility::redirect('index.php');
    }

    public function store(){
        $DBH = $this->conn;
        $data = array($this->name,$this->picture);
        $STH = $DBH->prepare("INSERT INTO `profile_picture`(`id`, `username`, `pro_pic`) VALUES (NULL ,?,?)");
        $STH->execute($data);
        Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ Picture: $this->picture] <br> Data Has Been Inserted Successfully!</h3></div>");
        Utility::redirect('create.php');


    }


}// end of BookTitle class