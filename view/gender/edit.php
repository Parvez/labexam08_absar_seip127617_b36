<?php
//var_dump($_GET);
include_once('../../vendor/autoload.php');
use App\Gender\Gender;
use App\Utility\Utility;

$obj= new Gender();
$obj->setData($_GET);
$oneData = $obj->view();
foreach($oneData as $value){
?>





<!DOCTYPE html>
<html lang="en">
<head>
    <title>CRUD-Gender</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Edit Gender</h2>
    <form role="form" method="post" action="update.php">
        <div class="form-group">
            <label>Edit Gender:</label>
            <input type="hidden" name="id" id="title" value="<?php echo $value->id;?>">
            <input type="text" name="name" class="form-control" id="name" placeholder="Enter Name" value="<?php echo $value->username;?>">
            <div class="col-sm-4">
                <div class="radio">
                    <label><input type="radio" name="gender" value="male" <?php if($value->gender =="male"){echo "checked";} ?>>Male</label>
                </div>
                <div class="radio">
                    <label><input type="radio" name="gender" value="female" <?php if($value->gender =="female"){echo "checked";} ?>>Female</label>
                </div>
            </div><?php } ?>
        </div>

        <button type="submit" value="submit" class="btn btn-default">Update</button>
    </form>
</div>

</body>
</html>
