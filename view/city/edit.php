<?php
include_once('../../vendor/autoload.php');
use App\City\City;
use App\Utility\Utility;

$obj= new City();
$obj->setData($_GET);
$oneData = $obj->view();

foreach($oneData as $singleItem) {
    $id = $singleItem->id;
    $name = $singleItem->username;
//Utility::d($singleItem);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <name></name>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <h2>Edit City</h2>
    <form role="form" method="post" action="update.php">
        <div class="form-group">
            <div class="col-sm-4">
            <label>Edit City Infromation:</label>
            <input type="hidden" name="id" id="name" value="<?php echo $id?>">
            <input type="text" name="username" class="form-control" id="name" placeholder="Enter name" value="<?php echo $name?>">
                <select name="city">
                    <option value="">Select</option>
                    <option value="dhaka" <?php if($singleItem->city_name=="dhaka")echo "selected";?>>Dhaka</option>
                    <option value="chittagong" <?php if($singleItem->city_name=="chittagong")echo "selected";?>>Chittagong</option>
                    <option value="khulna" <?php if($singleItem->city_name=="khulna")echo "selected";?>>Khulna</option>
                    <option value="barisal" <?php if($singleItem->city_name=="barisal")echo "selected";?>>Barisal</option>
                    <option value="sylet" <?php if($singleItem->city_name=="sylet")echo "selected";?>>Sylet</option>
                </select>              <?php } ?>
                <button type="submit" value="submit" class="btn btn-default">Update</button>
            </div>
        </div>
    </form>
</div>
</body>
</html>
